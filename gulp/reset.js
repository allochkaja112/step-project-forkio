const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');

module.exports = function reset() {
    return gulp.src("src/styles/reset.scss")
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({
            debug: true,
            compatibility: "*",
        }))
        .pipe(rename({
            suffix: '.min' }))
        .pipe(gulp.dest("dist/css"));
};